jQuery(function($) {

  var app = {

    owlInit: function() {

      $('.page-carousel__carousel').owlCarousel({

        items: 1,
        autoplay: true,
        dots: true,
        dotsContainer: '.page-carousel__dots .container'

      });

    },

    init: function() {

      this.owlInit();
      $('.filter__select').selectpicker({
      });


    }

  };

  app.init();

}(jQuery));