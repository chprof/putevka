var gulp			= require('gulp'),
	browserSync 	= require('browser-sync').create(),
	postcss			= require('gulp-postcss'),
	sourcemaps		= require('gulp-sourcemaps'),
	assets			= require('postcss-assets'),
	autoprefixer	= require('autoprefixer');
    cssnano         = require('cssnano'),
    mqpacker        = require('css-mqpacker'),
    sortCSSmq       = require('sort-css-media-queries'),
    cache           = require('gulp-cached-sass'),
    imagemin        = require('gulp-imagemin'),
    concat          = require('gulp-concat'),
    uglify          = require('gulp-uglify'),
	sass			= require('gulp-sass');


// Static Server + watching scss/html files

gulp.task('serve', ['sass'], function() {

    browserSync.init({
        server: "./"
    });

    gulp.watch("assets/scss/*.scss", ['sass']);
    gulp.watch("*.html").on('change', browserSync.reload);
});



// Compile sass into CSS & auto-inject into browsers

gulp.task('sass', function() {

	var processors = [
	  assets,
	  autoprefixer({browsers: ['last 40 version']})
	];

    return gulp.src("assets/scss/*style.scss")
        	.pipe(sourcemaps.init())
            .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
            .pipe(postcss(processors))
    		.pipe(sourcemaps.write('../maps'))
            .pipe(cache('sass'))
            .pipe(gulp.dest("assets/css"))
            .pipe(browserSync.stream());
});


gulp.task('css', function() {

    var processors = [
      assets,
      mqpacker({sort: sortCSSmq}),
      autoprefixer({browsers: ['last 40 version']}),
      cssnano
    ];

    return gulp.src("assets/scss/*style.scss")
            .pipe(sass())
            .pipe(postcss(processors))
            .pipe(gulp.dest("assets/css"))
});

gulp.task('img', () =>
    gulp.src('img/*')
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest('dist/images'))
);



gulp.task('deploy', () =>
    gulp.src('assets/**/*.*')
        .pipe(gulp.dest('dist'))
);

gulp.task('js', () =>
    gulp.src(['./assets/js/collapse.js', './assets/js/transition.js',
    './assets/js/ion.rangeSlider.min.js', './assets/js/iziModal.min.js',
    './assets/js/jquery.fancybox.min.js', './assets/js/jquery.maskedinput.js',
    './assets/js/app.js'])
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js'))
);

gulp.task('default', ['serve']);

